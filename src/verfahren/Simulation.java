package verfahren;

import gui.Fenster;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Simulation extends Thread{	
	private Fenster fenster;
	private int breite;
	private int hoehe;

	public Simulation(Fenster f){
		fenster = f;
		breite = fenster.getBreite();
		hoehe = fenster.getHoehe();
	}
	
	public void stoppen(){
		interrupt();
	}
	
	public void starten(){
		start();
	}

	public void run(){
		while( !isInterrupted() ){
			Platform.runLater(new Runnable(){
				
				@Override
				public void run(){
					if ( fenster.getStatustext() == fenster.getStatusfeld()[0] ||
						 fenster.getStatustext() == fenster.getStatusfeld()[2]){
						stoppen();
						Alert hinweis = new Alert(AlertType.INFORMATION);
						hinweis.setHeaderText("Raetsel geloest");
						hinweis.setContentText("Raetsel wurde geloest");
						hinweis.showAndWait();
					} else {
						simulation();
					}
				}
			});
			
			try {
				Thread.sleep(1000);
			} catch ( InterruptedException e){
				stoppen();
			}
		}
	}

	/** Sucht wie bei @ergaenzeSichereBruecke eine sichere Bruecke die zwischen 
	 *  zwei Inseln ergaenzt werden kann. Der Simulationsmodus arbeitet nur dann,
	 *  wenn sich der Status in den Zustand "noch nicht geloest" befindet
	 * 
	 */
	private void simulation(){
		Nachbarn n = new Nachbarn(fenster);
		
		for (int i = 0; i < breite; i++){
			for( int j = 0; j < hoehe; j++){
				if ( fenster.getInseln()[i][j] != null && fenster.getLoesung()[i][j] != null ){
					
					// ueberpruefe die ausgehenden Verbindungen nach Norden
					if ( fenster.getInseln()[i][j].anzahlVerbindungNord < fenster.getLoesung()[i][j].anzahlVerbindungNord){

						if ( n.getNordNachbar(i,j) < j && n.getNordNachbar(i,j) != j){
							fenster.getSpielfeld().verbindungHinzufuegen(fenster.getInseln()[i][j],fenster.getInseln()[i][n.getNordNachbar(i,j)]);
						}
						break;
						// ueberpruefe die ausgehenden Verbindungen nach Osten
					}
					
					if ( fenster.getInseln()[i][j].anzahlVerbindungOst < fenster.getLoesung()[i][j].anzahlVerbindungOst){
						
						if ( n.getOstNachbar(i,j) > i && n.getOstNachbar(i,j) != i) {
							fenster.getSpielfeld().verbindungHinzufuegen(fenster.getInseln()[i][j], fenster.getInseln()[n.getOstNachbar(i,j)][j]);
						}
						break;
						// ueberpruefe die ausgehenden Verbindungen nach Sueden
					}
					
					if ( fenster.getInseln()[i][j].anzahlVerbindungSued < fenster.getLoesung()[i][j].anzahlVerbindungSued){
						
						if ( n.getSuedNachbar(i,j) > j && n.getSuedNachbar(i,j) != j) {
							fenster.getSpielfeld().verbindungHinzufuegen(fenster.getInseln()[i][j],fenster.getInseln()[i][n.getSuedNachbar(i,j)]);
						}
						break;
						// ueberpruefe die ausgehenden Verbindungen nach Westen
					}

					if ( fenster.getInseln()[i][j].anzahlVerbindungWest < fenster.getLoesung()[i][j].anzahlVerbindungWest){
						
						if ( n.getWestNachbar(i,j) < i && n.getWestNachbar(i,j) != i) {
							fenster.getSpielfeld().verbindungHinzufuegen(fenster.getInseln()[i][j], fenster.getInseln()[n.getWestNachbar(i,j)][j]);
						}
						break;
					}
				}
			}
		}
	} // simulation ende
}
