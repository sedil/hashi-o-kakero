package verfahren;

import gui.Fenster;

public class Nachbarn {
	private Fenster fenster;
	private int breite, hoehe;
	
	public Nachbarn(Fenster f){
		fenster = f;
		breite = fenster.getBreite();
		hoehe = fenster.getHoehe();
	}
	
	/**
	 * Findet den noerdlichen Nachbarn der aktuellen Insel
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 * @return y-Koordinate der noerdlichen Insel, falls vorhanden
	 */
	public int getNordNachbar(int x, int y){
		
        for(int i = y - 1; i >= 0; i--) {
        	try {
        		if(fenster.getInseln()[x][i] != null) {
        			return i;
        		}
        	} catch ( ArrayIndexOutOfBoundsException e){
        		return y;
        	}
        }
        return y;
	}
	
	/**
	 * Findet den suedlichen Nachbarn der aktuellen Insel
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 * @return y-Koordinate der suedlichen Insel, falls vorhanden
	 */
	public int getSuedNachbar(int x, int y){
		
        for(int i = y + 1; i < hoehe; i++) {
        	try {
        		if(fenster.getInseln()[x][i] != null) {
        			return i;
        		}
        	} catch ( ArrayIndexOutOfBoundsException e){
        		return y;
        	}
        }
        return y;
	}
	
	/**
	 * Findet den westlichen Nachbarn der aktuellen Insel
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 * @return x-Koordinate der westlichen Insel, falls vorhanden
	 */
	public int getWestNachbar(int x, int y){
		
        for(int i = x - 1; i >= 0; i--) {
        	try {
        		if(fenster.getInseln()[i][y] != null) {
        			return i;
        		}
        	} catch ( ArrayIndexOutOfBoundsException e){
        		return x;
        	}
        }
		return x;
	}
	
	/**
	 * Findet den oestlichen Nachbarn der aktuellen Insel
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 * @return x-Koordinate der oestlichen Insel, falls vorhanden
	 */
	public int getOstNachbar(int x, int y){
		
        for(int i = x + 1; i < breite; i++) {
        	try {
        		if(fenster.getInseln()[i][y] != null) {
        			return i;
        		}
        	} catch ( ArrayIndexOutOfBoundsException e){
        		return x;
        	}
        }
        return x;
	}

}
