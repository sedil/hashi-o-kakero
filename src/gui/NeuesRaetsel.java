package gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Hier werden die Einstellungen vorgenommen, um ein neues Raetsel zu generieren.
 * Moegich sind ein Raetsel zufaellig erezugen zu lassen, die Breite und Hoehe selber zu waehlen,
 * oder aber die Breite, die Hoehe und die Anzahl der Inseln selber zu bestimmen
 */
public class NeuesRaetsel extends Stage {
	
	private Fenster fenster;
	private GridPane layout;
	private int b = 0;
	private int h = 0;
	private Label breite, hoehe;
	private TextField tBreite, tHoehe;
	private RadioButton auto;
	private Button abbruch, ok;
	
	/** Konstruktor initialisiert die Swing-Komponenten */
	public NeuesRaetsel(Fenster f){
		fenster = f;
		initModality(Modality.APPLICATION_MODAL);
		initOwner(fenster);
		initKomponenten();
		
		Scene inhalt = new Scene(layout);
		setScene(inhalt);
		sizeToScene();
		setTitle("Neues Raetsel");
		setResizable(false);
		showAndWait();
	}

	private void initKomponenten(){
		layout = new GridPane();
		/* Diverse Komponenten werden initialisiert */
		breite = new Label("Breite");
		tBreite = new TextField();
		hoehe = new Label("Hoehe");
		tHoehe = new TextField();
		auto = new RadioButton("zufaelliges Raetsel generieren");
		ok = new Button("OK");
		ok.setOnAction(new ButtonListener());
		auto.setOnAction(new ButtonListener());
		abbruch = new Button("Abbrechen");
		
		layout.add(breite, 0, 0);
		layout.add(tBreite, 1, 0);
		layout.add(hoehe, 0, 1);
		layout.add(tHoehe, 1, 1);
		layout.add(auto, 0, 2);
		layout.add(ok, 0, 3);
		layout.add(abbruch, 1, 3);
	}
	
	private void pruefeEingaben(){
			try {
				b = Integer.parseInt(tBreite.getText());
				h = Integer.parseInt(tHoehe.getText());
			} catch ( NumberFormatException ne){
				Alert f = new Alert(AlertType.WARNING);
				f.setHeaderText("Eingabefehler");
				f.setContentText("Nur numerische Eingaben erlaubt");
				f.showAndWait();
				b = 4;
				h = 4;
				tBreite.setText("4");
				tHoehe.setText("4");
				return;
			}
			if (!(b >= 4 && b <= 30 && h >= 4 && h <= 30 )) {
				Alert f = new Alert(AlertType.WARNING);
				f.setHeaderText("Eingabefehler");
				f.setContentText("Breite : 4 <= x <= 30\nHoehe : 4 <= x <= 30");
				f.showAndWait();
				b = 4;
				h = 4;
				tBreite.setText("4");
				tHoehe.setText("4");
			} else {
				fenster.setGrafikItem(true);
				close();
			}
		}
	
	private void automatik(){
		b = (int) (Math.random() * (30 - 4)+ 4);
		h = (int) (Math.random() * (30 - 4)+ 4);				
		fenster.setGrafikItem(true);
		close();
	}
	
	public int[] getRaetselgroesse(){
		return new int[]{b,h};
	}
	
	private class ButtonListener implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent ae) {
			if (auto.isSelected()){
				automatik();
			}
			// Nach Abbruch, aktuelles Fenster schliessen und zurÃ¼ck zum Hauptfenster
			if (ae.getSource() == abbruch){
				close();
				
			/* Nach ok, aktuelles Fenster schliessen und gueltige Werte an 
			 *	den Generator Uebergeben */
			} else if (ae.getSource() == ok){
				pruefeEingaben();
			}
		}
	} // ButtonListener ende
}
