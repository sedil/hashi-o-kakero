package hashi;

import gui.Fenster;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import verfahren.Nachbarn;

public class Bild extends Canvas {
	
	private Fenster fenster;
	private GraphicsContext gc;
	private boolean lmt; /* Linke Maus Taste */
	private boolean zeigeMax = true; /* Wechsel zwischen maximale Anzahl von Bruecken oder verbliebende Anzahl von Bruecken */
	private int startX;
	private int startY;
	private int endeX;
	private int endeY;
	private int[] fehlerindex;
	private boolean kreuz = false;
	
	/* Diese Attribute dienen zur Ermittlung des Mausklicksegments */
	private final int D = 28;
	private final int NORD = 0;
	private final int OST = 1;
	private final int SUED = 2;
	private final int WEST = 3;
	
	public Bild(Fenster f){
		fenster = f;
		setOnMouseClicked(new MausHandler());
		updateCanvasGroesse();
		gc = getGraphicsContext2D();
		fehlerindex = new int[2];
		zeichneKnoten();
	}
	
	public void updateCanvasGroesse(){
		setWidth(D*fenster.getBreite());
		setHeight(D*fenster.getHoehe());
	}
	
	void updateNachLaden(){
		for(int i = 0; i < fenster.getInseln().length; i++){
			for(int j = 0; j < fenster.getInseln()[i].length; j++){
				if(fenster.getInseln()[i][j] != null){
					
					/* Alle Inseldaten aktualisieren */
					fenster.getInseln()[i][j].anzahlVerbindungNord = fenster.getInseln()[i][j].getVerbindungNord();
					fenster.getInseln()[i][j].anzahlVerbindungOst = fenster.getInseln()[i][j].getVerbindungOst();
					fenster.getInseln()[i][j].anzahlVerbindungSued = fenster.getInseln()[i][j].getVerbindungSued();
					fenster.getInseln()[i][j].anzahlVerbindungWest = fenster.getInseln()[i][j].getVerbindungWest();
					fenster.getInseln()[i][j].aktuelleVerbindungen = fenster.getInseln()[i][j].getAktuelleAnzahlVerbindung();
					fenster.getInseln()[i][j].restVerbindungen = fenster.getInseln()[i][j].getRestVerbindungen();
					zeichnen();
				}
			}
		}	
	}
	
	/**
	 * Aendert, ob man die maximale Anzahl der Bruecken oder lieber die
	 * verbliebene Anzahl der Brucken sehen moechte
	 * 
	 * @param istAktiviert true, verbliebene Anzahl, false maximale Anzahl
	 */
	public void anzeigeMaxOderRestVerbindungen(boolean istAktiviert){
		zeigeMax = istAktiviert;
		zeichnen();
	}

	public void zeichnen(){
		gc.setFill(Color.WHITE);
		gc.fillRect(0,0,getWidth(), getHeight());

		zeichneLinie();
		zeichneKnoten();
	}

	/**
	 * Zeichnet alle vorhandenen Linien, markiert eine Kollision (Linienkreuzung) rot und
	 * faerbt die zuletzt eingezeichnete Linie Hellgruen
	 * @param g2d
	 */
	private void zeichneLinie(){
		if ( fenster.getLinien().isEmpty()){
			fenster.aktiviereSimulationButton(false);
			fenster.setGrafikItem(true);
		} else {
			fenster.aktiviereSimulationButton(true);
			fenster.setGrafikItem(false);
		}

		for(int i = 0; i < fenster.getLinien().size(); i++){
			gc.setStroke(Color.BLACK);
			gc.strokeLine(fenster.getLinien().get(i).getStartX(), fenster.getLinien().get(i).getStartY(), fenster.getLinien().get(i).getEndX(), fenster.getLinien().get(i).getEndY());
			gc.setStroke(Color.AQUA);
			gc.strokeLine(fenster.getLinien().get(fenster.getLinien().size()-1).getStartX(), fenster.getLinien().get(fenster.getLinien().size()-1).getStartY(), fenster.getLinien().get(fenster.getLinien().size()-1).getEndX(), fenster.getLinien().get(fenster.getLinien().size()-1).getEndY());
		}
		
		if ( kreuz ){
			gc.setStroke(Color.RED);
			gc.strokeLine(fenster.getLinien().get(fehlerindex[0]).getStartX(), fenster.getLinien().get(fehlerindex[0]).getStartY(), fenster.getLinien().get(fehlerindex[0]).getEndX(), fenster.getLinien().get(fehlerindex[0]).getEndY());
			gc.strokeLine(fenster.getLinien().get(fehlerindex[1]).getStartX(), fenster.getLinien().get(fehlerindex[1]).getStartY(), fenster.getLinien().get(fehlerindex[1]).getEndX(), fenster.getLinien().get(fehlerindex[1]).getEndY());
			kreuz = false;
		}
	}
	/**
	 * Zeichnet alle Knoten ein mit einer Zahl in der Mitte, wahlweise als
	 * Maximale Anzahl der ausgehenden Verbindungen oder als restliche
	 * Anzahl von ausgehenden Verbindungen
	 * @param g2d
	 */
	private void zeichneKnoten(){
		gc.setFill(Color.LIGHTGRAY);
		gc.setStroke(Color.BLACK);

		if ( zeigeMax == true) {
			for(int i = 0; i < fenster.getInseln().length; i++){
				for(int j = 0; j < fenster.getInseln()[i].length; j++){
					if ( fenster.getInseln()[i][j] != null && fenster.getInseln()[i][j].getRestVerbindungen() != 0){
						gc.setFill(Color.LIGHTGRAY);
						gc.fillOval(D*i, D*j, D, D);
						gc.strokeText(""+fenster.getInseln()[i][j].getMaxAnzahlVerbindung(), 28*(i+1) - 17 , 28*(j+1) - 10);
					}
					if (fenster.getInseln()[i][j] != null && fenster.getInseln()[i][j].getRestVerbindungen() == 0){
						gc.setFill(Color.GREENYELLOW);
						gc.fillOval(D*i, D*j, D, D);
						gc.strokeText(""+fenster.getInseln()[i][j].getMaxAnzahlVerbindung(), 28*(i+1) - 17 , 28*(j+1) - 10);
					}
				}
			}
		} else {
			for(int i = 0; i < fenster.getInseln().length; i++){
				for(int j = 0; j < fenster.getInseln()[i].length; j++){
					if ( fenster.getInseln()[i][j] != null && fenster.getInseln()[i][j].getRestVerbindungen() != 0){
						gc.setFill(Color.LIGHTGRAY);
						gc.fillOval(D*i, D*j, D, D);
						gc.strokeText(""+fenster.getInseln()[i][j].getRestVerbindungen(), 28*(i+1) - 17 , 28*(j+1) - 10);
					}
					if ( fenster.getInseln()[i][j] != null && fenster.getInseln()[i][j].getRestVerbindungen() == 0){
						gc.setFill(Color.GREENYELLOW);
						gc.fillOval(D*i, D*j, D, D);
						gc.strokeText(""+fenster.getInseln()[i][j].getRestVerbindungen(), 28*(i+1) - 17 , 28*(j+1) - 10);
					}
				}
			}
		}
	}

	/**
	 * Prueft, auf welche Insel geklickt wurde, ermittelt die Koordinaten und ermittelt ob mit
	 * der linken oder rechten Maustaste geklickt wurde
	 * 
	 * @param px				Mauskoordinaten in Pixel
	 * @param py				Mauskoordinaten in Pixel
	 * @param zelleX			Inselkoordinate als x
	 * @param zelleY			Inselkoordinate als y
	 * @param linkeMausTaste	true = linke Maustaste ; false = rechte Maustaste
	 */
	private void analysiereKlickSegment(int px, int py, int zelleX, int zelleY, boolean istLinkeMausTaste){
		int x = px;
		int y = py;
		lmt = istLinkeMausTaste;
	
		/* Nord */
		if ( zelleX*D + 4 <= x && x <= zelleX*D + 24 && zelleY*D <= y && y <= zelleY*D + 10){
			findeNachbar(zelleX,zelleY,NORD,lmt);
			
		/* Ost */
		} else if ( zelleX*D + 20 <= x && x <= zelleX*D + D && zelleY*D + 8 <= y && y <= zelleY*D + 24){
			findeNachbar(zelleX,zelleY,OST,lmt);
			
		/* Sued */
		} else if ( zelleX*D + 4 <= x && x <= zelleX*D + 24 && zelleY*D + 20 <= y && y <= zelleY*D + D){
			findeNachbar(zelleX,zelleY,SUED,lmt);
			
		/* West */
		} else if ( zelleX*D <= x && x <= zelleX*D + 10 && zelleY*D + 4 <= y && y<= zelleY*D + 24){
			findeNachbar(zelleX,zelleY,WEST,lmt);
		}

	}
	
	/**
	 * Findet die Nachbarinsel ausgehend vom Ergebnis aus @analysiereKlickSegment
	 * 
	 * @param x					aktuelle Inselkoordinate
	 * @param y					aktuelle Inselkoordinate
	 * @param richtung			Ermittelte Richtung aus @analysiereKlickSegment
	 * @param linkeMausTaste	Ermittelte Maustaste aus @analyisereKlickSegment
	 */
	private void findeNachbar(int x, int y, final int richtung, boolean istLinkeMausTaste){
		Nachbarn n = new Nachbarn(fenster);
		int posx = x;
		int posy = y;
		final int r = richtung;
		lmt = istLinkeMausTaste;
		
		try {
			if ( lmt == true ){
				if ( r == NORD ){
					// Ist y-Koordinate gueltig UND sind beide y-Koordinaten verschieden ?					
					if ( n.getNordNachbar(posx,posy) != posy && fenster.getInseln()[x][n.getNordNachbar(posx,posy)].posy >= 0 &&
							fenster.getInseln()[x][n.getNordNachbar(posx,posy)].posy < fenster.getInseln()[x][y].posy-1) {
					     	startX = posx;
					     	startY = posy;
					     	endeX = fenster.getInseln()[x][n.getNordNachbar(posx,posy)].posx;
					     	endeY = fenster.getInseln()[x][n.getNordNachbar(posx,posy)].posy;
					     	verbindungHinzufuegen(fenster.getInseln()[posx][posy],fenster.getInseln()[x][n.getNordNachbar(posx,posy)]);
					}
				}
			
				if ( r == OST ){
					// Ist x-Koordinate gueltig UND sind beide x-Koordinaten verschieden ?
					if ( n.getOstNachbar(posx,posy) != posx && fenster.getInseln()[n.getOstNachbar(posx,posy)][y].posx < fenster.getBreite() &&
							fenster.getInseln()[n.getOstNachbar(posx,posy)][y].posx > fenster.getInseln()[x][y].posx+1){
					     	startX = posx;
					     	startY = posy;
					     	endeX = fenster.getInseln()[n.getOstNachbar(posx,posy)][y].posx;
					     	endeY = fenster.getInseln()[n.getOstNachbar(posx,posy)][y].posy;
					     	verbindungHinzufuegen(fenster.getInseln()[posx][posy],fenster.getInseln()[n.getOstNachbar(posx,posy)][y]);
					}
				}
			
				if ( r == SUED ){
					// Ist y-Koordinate gueltig UND sind beide y-Koordinaten verschieden ?
					if ( n.getSuedNachbar(posx,posy) != posy && fenster.getInseln()[x][n.getSuedNachbar(posx,posy)].posy < fenster.getHoehe() &&
							fenster.getInseln()[x][n.getSuedNachbar(posx,posy)].posy > fenster.getInseln()[x][y].posy+1){
					     	startX = posx;
					     	startY = posy;
					     	endeX = fenster.getInseln()[x][n.getSuedNachbar(posx,posy)].posx;
					     	endeY = fenster.getInseln()[x][n.getSuedNachbar(posx,posy)].posy;
					     	verbindungHinzufuegen(fenster.getInseln()[posx][posy],fenster.getInseln()[x][n.getSuedNachbar(posx,posy)]);
					}
				}
			
				if ( r == WEST ){
					// Ist x-Koordinate gueltig UND sind beide x-Koordinaten verschieden ?
					if ( n.getWestNachbar(posx,posy) != posx && fenster.getInseln()[n.getWestNachbar(posx,posy)][y].posx >= 0 &&
							fenster.getInseln()[n.getWestNachbar(posx,posy)][y].posx < fenster.getInseln()[x][y].posx-1){
					     	startX = posx;
					     	startY = posy;
					     	endeX = fenster.getInseln()[n.getWestNachbar(posx,posy)][y].posx;
					     	endeY = fenster.getInseln()[n.getWestNachbar(posx,posy)][y].posy;
					     	verbindungHinzufuegen(fenster.getInseln()[posx][posy],fenster.getInseln()[n.getWestNachbar(posx,posy)][y]);
					}
				}
			} else if ( lmt == false ){
					if ( r == NORD ){
						// Ist y-Koordinate gueltig UND sind beide y-Koordinaten verschieden ?
						if ( n.getNordNachbar(posx,posy) != posy && fenster.getInseln()[x][n.getNordNachbar(posx,posy)].posy >= 0 &&
								fenster.getInseln()[x][n.getNordNachbar(posx,posy)].posy < fenster.getInseln()[x][y].posy-1){
								startX = posx;
								startY = posy;
								endeX = fenster.getInseln()[x][n.getNordNachbar(posx,posy)].posx;
								endeY = fenster.getInseln()[x][n.getNordNachbar(posx,posy)].posy;
								verbindungEntfernen(fenster.getInseln()[posx][posy],fenster.getInseln()[x][n.getNordNachbar(posx,posy)]);
						}
					}
			
					if ( r == OST ){
						// Ist x-Koordinate gueltig UND sind beide x-Koordinaten verschieden ?
						if ( n.getOstNachbar(posx,posy) != posx && fenster.getInseln()[n.getOstNachbar(posx,posy)][y].posx < fenster.getBreite() &&
								fenster.getInseln()[n.getOstNachbar(posx,posy)][y].posx > fenster.getInseln()[x][y].posx+1){
								startX = posx;
								startY = posy;
								endeX = fenster.getInseln()[n.getOstNachbar(posx,posy)][y].posx;
						     	endeY = fenster.getInseln()[n.getOstNachbar(posx,posy)][y].posy;
						     	verbindungEntfernen(fenster.getInseln()[posx][posy],fenster.getInseln()[n.getOstNachbar(posx,posy)][y]);
						}
					}
			
					if ( r == SUED ){
						// Ist y-Koordinate gueltig UND sind beide y-Koordinaten verschieden ?
						if ( n.getSuedNachbar(posx,posy) != posy && fenster.getInseln()[x][n.getSuedNachbar(posx,posy)].posy < fenster.getHoehe() &&
								fenster.getInseln()[x][n.getSuedNachbar(posx,posy)].posy > fenster.getInseln()[x][y].posy+1){
						     	startX = posx;
						     	startY = posy;
						     	endeX = fenster.getInseln()[x][n.getSuedNachbar(posx,posy)].posx;
						     	endeY = fenster.getInseln()[x][n.getSuedNachbar(posx,posy)].posy;
						     	verbindungEntfernen(fenster.getInseln()[posx][posy],fenster.getInseln()[x][n.getSuedNachbar(posx,posy)]);
						}
					}
			
					if ( r == WEST ){
						// Ist x-Koordinate gueltig UND sind beide x-Koordinaten verschieden ?
						if ( n.getWestNachbar(posx,posy) != posx && fenster.getInseln()[n.getWestNachbar(posx,posy)][y].posx >= 0 &&
								fenster.getInseln()[n.getWestNachbar(posx,posy)][y].posx < fenster.getInseln()[x][y].posx-1){
						     	startX = posx;
						     	startY = posy;
						     	endeX = fenster.getInseln()[n.getWestNachbar(posx,posy)][y].posx;
						     	endeY = fenster.getInseln()[n.getWestNachbar(posx,posy)][y].posy;
						     	verbindungEntfernen(fenster.getInseln()[posx][posy],fenster.getInseln()[n.getWestNachbar(posx,posy)][y]);
						}
					}
				}
		
		} catch ( NullPointerException npe ){}
	}
	
	/**
     * Entfernt eine Bruecke zwischen zwei beteiligen Inseln (insel1, insel2). Es wird
     * u.a. geprueft ob Inseln existieren, ob beide Inseln dieselben Koordinaten besitzen
     * und ob die beiden Inseln vertikal (NORD, SUED) oder horizontal (WEST, OST) ausgerichtet
     * sind.
     * 
     * Je nach Situation werden die Anzahl der Verbindungen aktualisiert, wobei beim entfernen
     * einer Bruecke die Anzahl der Himmelsrichtung dekrementiert wird. Es werden beide 
     * beteiligten Inseln aktualisiert.
     *
     * @param x1 x-Koordinate von der ersten Insel
     * @param y1 y-Koordinate von der ersten Insel
     * @param x2 x-Koordinate von der zweiten Insel
     * @param y2 x-Koordinate von der zweiten Insel
     * @return false, im Falle eines Verstosses
     *         true, im Falle keines Verstosses
     */	
	private boolean verbindungEntfernen(Insel start, Insel ende)
    {
    	Insel insel1 = null;
    	Insel insel2 = null;
    	Nachbarn n = new Nachbarn(fenster);

    	/* Pruefe, ob Inseln existieren */
        try
        {
            insel1 = fenster.getInseln()[start.posx][start.posy];
            insel2 = fenster.getInseln()[ende.posx][ende.posy];
        }
        catch(IndexOutOfBoundsException e)
        {
            return false;
        }

        /* Pruefe, ob einer der beiden Inseln nicht existiert */
        if(insel1 == null || insel2 == null)
        {
            return false;
        }

        /* Die Inseln sind Vertikal angeordnet (NORD, SUED) oder (SUED, NORD) */
        if(start.posx == ende.posx)
        {
            if(start.posy == ende.posy)
            {
                return false; /* Inseln haben gleiche Koordinaten */
            }
            else if(start.posy < ende.posy)
            {
                if(insel1.anzahlVerbindungSued > 0 && insel2.anzahlVerbindungNord > 0 && ende.posy == n.getSuedNachbar(start.posx, start.posy))
                {
                    insel1.anzahlVerbindungSued--; /* Anzahl der Suedbruecke  -1 von Insel 1 */
                    insel2.anzahlVerbindungNord--; /* Anzahl der Nordbruecke -1 von Insel 2 */
                    insel1.aktuelleVerbindungen = insel1.getAktuelleAnzahlVerbindung();
                    insel2.aktuelleVerbindungen = insel2.getAktuelleAnzahlVerbindung();
                    insel1.restVerbindungen = insel1.getRestVerbindungen();
                    insel2.restVerbindungen = insel2.getRestVerbindungen();
                    
                    if ( insel1.anzahlVerbindungSued == 0 && insel2.anzahlVerbindungNord == 0){
						for(int i = 0; i < fenster.getLinien().size(); i++){
							if (fenster.getLinien().get(i).getStartX() == (int) D/2*(2*startX+1) && fenster.getLinien().get(i).getStartY() == D*(startY+1) && fenster.getLinien().get(i).getEndX() == fenster.getLinien().get(i).getStartX() && fenster.getLinien().get(i).getEndY() == D*(endeY+1)-D){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
						}
                    } else if ( insel1.anzahlVerbindungSued == 1 && insel2.anzahlVerbindungNord == 1){
                    	fenster.addLinie(new Line((int) D/2*(2*startX+1), D*(startY+1), (int) D/2*(2*startX+1), D*(endeY+1)-D));
						
						for(int i = 0; i < fenster.getLinien().size(); i++){
							if (fenster.getLinien().get(i).getStartX() == (int) D/2*(2*startX+1)-4 && fenster.getLinien().get(i).getStartY() == D*(startY+1) && fenster.getLinien().get(i).getEndX() == fenster.getLinien().get(i).getStartX() && fenster.getLinien().get(i).getEndY() == D*(endeY+1)-D){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
							if (fenster.getLinien().get(i).getStartX() == (int) D/2*(2*startX+1)+4 && fenster.getLinien().get(i).getStartY() == D*(startY+1) && fenster.getLinien().get(i).getEndX() == fenster.getLinien().get(i).getStartX() && fenster.getLinien().get(i).getEndY() == D*(endeY+1)-D){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
						}
                    }     
                }
                else
                {
                    return false;
                }
            }
            else /* Umgedrehter Fall, falls zuerst die suedliche der beiden Inseln angewaehlt wird */
            {
                if(insel2.anzahlVerbindungSued > 0 && insel1.anzahlVerbindungNord > 0 && start.posy == n.getSuedNachbar(start.posx, ende.posy))
                {
                    insel2.anzahlVerbindungSued--;
                    insel1.anzahlVerbindungNord--;
                    insel1.aktuelleVerbindungen = insel1.getAktuelleAnzahlVerbindung();
                    insel2.aktuelleVerbindungen = insel2.getAktuelleAnzahlVerbindung();
                    insel1.restVerbindungen = insel1.getRestVerbindungen();
                    insel2.restVerbindungen = insel2.getRestVerbindungen();
                    
                    if ( insel1.anzahlVerbindungNord == 0 && insel2.anzahlVerbindungSued == 0){
						for(int i = 0; i < fenster.getLinien().size(); i++){
							if (fenster.getLinien().get(i).getStartX() == (int) D/2*(2*startX+1) && fenster.getLinien().get(i).getStartY() == D*(endeY+1) && fenster.getLinien().get(i).getEndX() == fenster.getLinien().get(i).getStartX() && fenster.getLinien().get(i).getEndY() == D*(startY+1)-D){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
						}
                    } else if ( insel1.anzahlVerbindungNord == 1 && insel2.anzahlVerbindungSued == 1){
                    	fenster.addLinie(new Line((int) D/2*(2*startX+1), D*(endeY+1), (int) D/2*(2*startX+1),D*(startY+1)-D));
						
						for(int i = 0; i < fenster.getLinien().size(); i++){
							if (fenster.getLinien().get(i).getStartX() == (int) D/2*(2*startX+1)-4 && fenster.getLinien().get(i).getStartY() == D*(endeY+1) && fenster.getLinien().get(i).getEndX() ==fenster.getLinien().get(i).getStartX() && fenster.getLinien().get(i).getEndY() == D*(startY+1)-D){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
							if (fenster.getLinien().get(i).getStartX() == (int) D/2*(2*startX+1)+4 && fenster.getLinien().get(i).getStartY() == D*(endeY+1) && fenster.getLinien().get(i).getEndX() == fenster.getLinien().get(i).getStartX() && fenster.getLinien().get(i).getEndY() == D*(startY+1)-D){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
						}
                    }                 
                }
                else
                {
                    return false;
                }
            }
        }
        
        /* Die Inseln sind Horizonatl angeordnet (WEST, OST) oder (OST, WEST) */
        else if(start.posy == ende.posy) 
        {
            if(start.posx > ende.posx)
            {
                if(insel1.anzahlVerbindungWest > 0 && insel2.anzahlVerbindungOst > 0 && start.posx == n.getOstNachbar(ende.posx, ende.posy))
                {
                    insel1.anzahlVerbindungWest--; /* Anzahl der Westbruecke -1 von Insel 1 */
                    insel2.anzahlVerbindungOst--;  /* Anzahl der Ostbruecke  -1 von Insel 2 */
                    insel1.aktuelleVerbindungen = insel1.getAktuelleAnzahlVerbindung();
                    insel2.aktuelleVerbindungen = insel2.getAktuelleAnzahlVerbindung();
                    insel1.restVerbindungen = insel1.getRestVerbindungen();
                    insel2.restVerbindungen = insel2.getRestVerbindungen();
                    
                    if ( insel1.anzahlVerbindungWest == 0 && insel2.anzahlVerbindungOst == 0){
						for(int i = 0; i < fenster.getLinien().size(); i++){
							if (fenster.getLinien().get(i).getStartX() == D*(endeX+1) && fenster.getLinien().get(i).getStartY() == (int) D/2*(2*startY+1) && fenster.getLinien().get(i).getEndX() == D*(startX+1)-D && fenster.getLinien().get(i).getEndY() == fenster.getLinien().get(i).getStartY()){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
						}
                    } else if ( insel1.anzahlVerbindungWest == 1 && insel2.anzahlVerbindungOst == 1){
                    	fenster.addLinie(new Line(D*(endeX+1), (int) D/2*(2*startY+1), D*(startX+1)-D, (int) D/2*(2*startY+1)));
						
						for(int i = 0; i < fenster.getLinien().size(); i++){
							if (fenster.getLinien().get(i).getStartX() == D*(endeX+1) && fenster.getLinien().get(i).getStartY() == (int) D/2*(2*startY+1)-4 && fenster.getLinien().get(i).getEndX() == D*(startX+1)-D && fenster.getLinien().get(i).getEndY() == fenster.getLinien().get(i).getStartY()){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
							if (fenster.getLinien().get(i).getStartX() == D*(endeX+1) && fenster.getLinien().get(i).getStartY() == (int) D/2*(2*startY+1)+4 && fenster.getLinien().get(i).getEndX() == D*(startX+1)-D && fenster.getLinien().get(i).getEndY() == fenster.getLinien().get(i).getStartY()){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
						}
                    }
                }
                else
                {
                    return false;
                }
            }
            else /* Umgedrehter Fall, falls zuerst die Oestliche der beiden Inseln angewaehlt wird */
            {
                if(insel1.anzahlVerbindungOst > 0 && insel2.anzahlVerbindungWest > 0 && ende.posx == n.getOstNachbar(start.posx, start.posy))
                {
                    insel1.anzahlVerbindungOst--;
                    insel2.anzahlVerbindungWest--;
                    insel1.aktuelleVerbindungen = insel1.getAktuelleAnzahlVerbindung();
                    insel2.aktuelleVerbindungen = insel2.getAktuelleAnzahlVerbindung();
                    insel1.restVerbindungen = insel1.getRestVerbindungen();
                    insel2.restVerbindungen = insel2.getRestVerbindungen();
                    
                    
                    if ( insel1.anzahlVerbindungOst == 0 && insel2.anzahlVerbindungWest == 0){
						for(int i = 0; i < fenster.getLinien().size(); i++){
							if (fenster.getLinien().get(i).getStartX() == D*(startX+1) && fenster.getLinien().get(i).getStartY() == (int) D/2*(2*startY+1) && fenster.getLinien().get(i).getEndX() == D*(endeX+1)-D && fenster.getLinien().get(i).getEndY() == fenster.getLinien().get(i).getStartY()){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
						}
                    } else if ( insel1.anzahlVerbindungOst == 1 && insel2.anzahlVerbindungWest == 1){
                    	fenster.addLinie(new Line(D*(startX+1), (int) D/2*(2*startY+1), D*(endeX+1)-D, (int) D/2*(2*startY+1)));					
						
						for(int i = 0; i < fenster.getLinien().size(); i++){
							if (fenster.getLinien().get(i).getStartX() == D*(startX+1) && fenster.getLinien().get(i).getStartY() == (int) D/2*(2*startY+1)-4 && fenster.getLinien().get(i).getEndX() == D*(endeX+1)-D && fenster.getLinien().get(i).getEndY()== fenster.getLinien().get(i).getStartY()){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
							if (fenster.getLinien().get(i).getStartX() == D*(startX+1) && fenster.getLinien().get(i).getStartY() == (int) D/2*(2*startY+1)+4 && fenster.getLinien().get(i).getEndX() == D*(endeX+1)-D && fenster.getLinien().get(i).getEndY() == fenster.getLinien().get(i).getStartY()){
								fenster.entferneLinie(fenster.getLinien().get(i));
							}
						}
                    }  
                } else{
                    return false;
                }
            }
        }
        
        /* Pruefe Status */
        updateStatus();
        return true;
    }

    /**
     * Fuegt eine Bruecke zwischen zwei beteiligen Inseln (insel1, insel2). Genau wie bei
     * @verbindungEntfernen wird geprueft ob Inseln existieren, ob beide Inseln dieselben Koordinaten besitzen
     * und ob die beiden Inseln vertikal (NORD, SUED) oder horizontal (WEST, OST) ausgerichtet
     * sind.
     * 
     * Je nach Situation werden die Anzahl der Verbindungen aktualisiert, wobei beim hinzufuegen
     * einer Bruecke die Anzahl der Himmelsrichtung inkrementiert wird. Es werden beide 
     * beteiligten Inseln aktualisiert.
     *
     * @param x1 x-Koordinate von der ersten Insel
     * @param y1 y-Koordinate von der ersten Insel
     * @param x2 x-Koordinate von der zweiten Insel
     * @param y2 x-Koordinate von der zweiten Insel
     * @return false, im Falle eines Verstosses
     *         true, im Falle keines Verstosses
     */
	public boolean verbindungHinzufuegen(Insel start, Insel ende) {
    	/* Da der Simulator / Brueckenmodus diese Methode aufruft, muessen
    	 * die globalen Variablen ueberschrieben werden */
		startX = start.posx;
    	startY = start.posy;
    	endeX = ende.posx;
    	endeY = ende.posy;
    	
		Insel insel1 = null;
    	Insel insel2 = null;

    	/* Pruefe, ob Inseln existieren */
        try
        {
            insel1 = fenster.getInseln()[start.posx][start.posy];
            insel2 = fenster.getInseln()[ende.posx][ende.posy];
        }
        catch(IndexOutOfBoundsException e)
        {
            return false;
        }

        /* Pruefe, ob einer der beiden Inseln nicht existiert */
        if(insel1 == null || insel2 == null)
        {
            return false;
        }

        /* Es wird verhindert, dass man mehr als die maximale Anzahl der Bruecken einer Insel hinzufuegt */
        if(insel1.maxAnzahlVerbindungen == (insel1.anzahlVerbindungSued + insel1.anzahlVerbindungNord + insel1.anzahlVerbindungOst + insel1.anzahlVerbindungWest) ||
            insel2.maxAnzahlVerbindungen == (insel2.anzahlVerbindungSued + insel2.anzahlVerbindungNord + insel2.anzahlVerbindungOst + insel2.anzahlVerbindungWest))
        {
            return false;
        }
        
        /* Die Inseln sind Vertikal angeordnet (NORD, SUED) oder (SUED, NORD) */  
        if(start.posx == ende.posx)
        {
            if(start.posy == ende.posy)
            {
                return false; /* Inseln haben gleiche Koordinaten */
            }
            else if(start.posy < ende.posy)
            {
                for(int i = start.posy + 1; i < ende.posy; i++) // WAS PASSIERT HIER ???
                {
                    if(fenster.getInseln()[start.posx][i] != null)
                    {
                        return false;
                    }
                }

                /* Nicht mehr als 2 Bruecken zwischen zwei Inseln */
                if(insel1.anzahlVerbindungSued >= 2 || insel2.anzahlVerbindungNord >= 2)
                {
                    return false;
                }
                /* Falls vorher 0 oder 1 Bruecke zwischen Inseln existierte */
                insel1.anzahlVerbindungSued++;
                insel2.anzahlVerbindungNord++;
                insel1.aktuelleVerbindungen = insel1.getAktuelleAnzahlVerbindung();
                insel2.aktuelleVerbindungen = insel2.getAktuelleAnzahlVerbindung();
                insel1.restVerbindungen = insel1.getRestVerbindungen();
                insel2.restVerbindungen = insel2.getRestVerbindungen();

                if ( insel1.anzahlVerbindungSued == 2 && insel2.anzahlVerbindungNord == 2){
                	fenster.addLinie(new Line((int) D/2*(2*startX+1)-4, D*(startY+1), (int) D/2*(2*startX+1)-4, D*(endeY+1)-D));
                	fenster.addLinie(new Line((int) D/2*(2*startX+1)+4, D*(startY+1), (int) D/2*(2*startX+1)+4, D*(endeY+1)-D));	
				
					for(int i = 0; i < fenster.getLinien().size(); i++){
						if (fenster.getLinien().get(i).getStartX() == (int) D/2*(2*startX+1) && fenster.getLinien().get(i).getStartY() == D*(startY+1) && fenster.getLinien().get(i).getEndX() == fenster.getLinien().get(i).getStartX() && fenster.getLinien().get(i).getEndY() == D*(endeY+1)-D){
							fenster.entferneLinie(fenster.getLinien().get(i));
						}
					}
                } else if ( insel1.anzahlVerbindungSued == 1 && insel2.anzahlVerbindungNord == 1){
                	fenster.addLinie(new Line((int) D/2*(2*startX+1), D*(startY+1), (int) D/2*(2*startX+1), D*(endeY+1)-D));
                }                
            }
            else // y2 > y1
            {
                for(int i = ende.posy + 1; i < start.posy; i++)
                {
                    if(fenster.getInseln()[start.posx][i] != null)
                    {
                        return false;
                    }
                }
                // Nicht mehr als 2 Bruecken zwischen zwei Inseln //
                if(insel2.anzahlVerbindungSued >= 2 || insel1.anzahlVerbindungNord >= 2)
                {
                    return false;
                }
                // Falls vorher 0 oder 1 Bruecke zwischen Inseln existierte //
                insel2.anzahlVerbindungSued++;
                insel1.anzahlVerbindungNord++;
                insel1.aktuelleVerbindungen = insel1.getAktuelleAnzahlVerbindung();
                insel2.aktuelleVerbindungen = insel2.getAktuelleAnzahlVerbindung();
                insel1.restVerbindungen = insel1.getRestVerbindungen();
                insel2.restVerbindungen = insel2.getRestVerbindungen();
                
                if ( insel1.anzahlVerbindungNord == 2 && insel2.anzahlVerbindungSued == 2){
                	fenster.addLinie(new Line((int) D/2*(2*startX+1)-4, D*(endeY+1), (int) D/2*(2*startX+1)-4, D*(startY+1)-D));
                	fenster.addLinie(new Line((int) D/2*(2*startX+1)+4, D*(endeY+1), (int) D/2*(2*startX+1)+4, D*(startY+1)-D));
				
					for(int i = 0; i < fenster.getLinien().size(); i++){
						if (fenster.getLinien().get(i).getStartX() == (int) D/2*(2*startX+1) && fenster.getLinien().get(i).getStartY() == D*(endeY+1) && fenster.getLinien().get(i).getEndX() == fenster.getLinien().get(i).getStartX() && fenster.getLinien().get(i).getEndY() == D*(startY+1)-D){
							fenster.entferneLinie(fenster.getLinien().get(i));
						}
					}
                } else if ( insel1.anzahlVerbindungNord == 1 && insel2.anzahlVerbindungSued == 1){
                	fenster.addLinie(new Line((int) D/2*(2*startX+1), D*(endeY+1), (int) D/2*(2*startX+1),D*(startY+1)-D));
                }
            }
        } 
        /* Die Inseln sind Horizonatl angeordnet (WEST, OST) oder (OST, WEST) */
        else if(start.posy == ende.posy)
        {
            if(start.posx < ende.posx)
            {
                for(int i = start.posx + 1; i < ende.posx; i++) // WAS PASSIERT HIER ???
                {
                    if(fenster.getInseln()[i][start.posy] != null)
                    {
                        return false;
                    }
                } 
                
                /* Nicht mehr als 2 Bruecken zwischen zwei Inseln */
                if(insel1.anzahlVerbindungOst >= 2 || insel2.anzahlVerbindungWest >= 2)
                {
                    return false;
                }
                /* Falls vorher 0 oder 1 Bruecke zwischen Inseln existierte */
                insel1.anzahlVerbindungOst++;
                insel2.anzahlVerbindungWest++;
                insel1.aktuelleVerbindungen = insel1.getAktuelleAnzahlVerbindung();
                insel2.aktuelleVerbindungen = insel2.getAktuelleAnzahlVerbindung();
                insel1.restVerbindungen = insel1.getRestVerbindungen();
                insel2.restVerbindungen = insel2.getRestVerbindungen();
                
                if ( insel1.anzahlVerbindungOst == 2 && insel2.anzahlVerbindungWest == 2){
                	fenster.addLinie(new Line(D*(startX+1), (int) D/2*(2*startY+1)-4, D*(endeX+1)-D, (int) D/2*(2*startY+1)-4));
					fenster.addLinie(new Line(D*(startX+1), (int) D/2*(2*startY+1)+4, D*(endeX+1)-D, (int) D/2*(2*startY+1)+4));
				
					for(int i = 0; i < fenster.getLinien().size(); i++){
						if (fenster.getLinien().get(i).getStartX() == D*(startX+1) && fenster.getLinien().get(i).getStartY() == (int) D/2*(2*startY+1) && fenster.getLinien().get(i).getEndX() == D*(endeX+1)-D && fenster.getLinien().get(i).getEndY() == fenster.getLinien().get(i).getStartY()){
							fenster.entferneLinie(fenster.getLinien().get(i));
						}
					}
                } else if ( insel1.anzahlVerbindungOst == 1 && insel2.anzahlVerbindungWest == 1) {
                	fenster.addLinie(new Line(D*(startX+1), (int) D/2*(2*startY+1), D*(endeX+1)-D, (int) D/2*(2*startY+1)));
                }  
            }
            else // x2 > x1
            {
                for(int i = ende.posx + 1; i < start.posx; i++)
                {
                    if(fenster.getInseln()[i][start.posy] != null)
                    {
                        return false;
                    }
                }
                /* Nicht mehr als 2 Bruecken zwischen zwei Inseln */
                if(insel2.anzahlVerbindungOst >= 2 || insel1.anzahlVerbindungWest >= 2)
                {
                    return false;
                }
                /* Falls vorher 0 oder 1 Bruecke zwischen Inseln existierte */
                insel2.anzahlVerbindungOst++;
                insel1.anzahlVerbindungWest++;
                insel1.aktuelleVerbindungen = insel1.getAktuelleAnzahlVerbindung();
                insel2.aktuelleVerbindungen = insel2.getAktuelleAnzahlVerbindung();
                insel1.restVerbindungen = insel1.getRestVerbindungen();
                insel2.restVerbindungen = insel2.getRestVerbindungen();
                
                if (insel1.anzahlVerbindungWest == 2 && insel2.anzahlVerbindungOst == 2){
                	fenster.addLinie(new Line(D*(endeX+1), (int) D/2*(2*startY+1)-4, D*(startX+1)-D, (int) D/2*(2*startY+1)-4));
                	fenster.addLinie(new Line(D*(endeX+1), (int) D/2*(2*startY+1)+4, D*(startX+1)-D, (int) D/2*(2*startY+1)+4));
					
					for(int i = 0; i < fenster.getLinien().size(); i++){
						if (fenster.getLinien().get(i).getStartX() == D*(endeX+1) && fenster.getLinien().get(i).getStartY() == (int) D/2*(2*startY+1) &&fenster.getLinien().get(i).getEndX() == D*(startX+1)-D && fenster.getLinien().get(i).getEndY() == fenster.getLinien().get(i).getStartY()){
							fenster.entferneLinie(fenster.getLinien().get(i));
						}
					}
                } else if (insel1.anzahlVerbindungWest == 1 && insel2.anzahlVerbindungOst == 1){
                	fenster.addLinie(new Line(D*(endeX+1), (int) D/2*(2*startY+1), D*(startX+1)-D, (int) D/2*(2*startY+1)));
                }
            }
        }
        /* Pruefe Status */ 
        updateStatus();
        return true;
    }
	
	private void updateStatus(){
    	istGeloest();
    	istUngeloest();
    	hatFehler();
    	zeichnen();
	}
    
    /**
     *  Prueft, ob das aktuelle Raetsel einen Fehler enthaelt
     *  
     *  - mindestens eine Linie kreuzt sich mit einer beliebigen anderen Linie
     */
    private void hatFehler(){
    	if ( fenster.getLinien().size() > 1){
        	for(int i = 0; i < fenster.getLinien().size() - 1; i++){
        		for(int j = i+1; j < fenster.getLinien().size(); j++) {
        			if ( schneidet(fenster.getLinien().get(i),fenster.getLinien().get(j)) ){
        				kreuz = true;
        				fehlerindex[0] = i;
        				fehlerindex[1] = j;
        				Alert hinweis = new Alert(AlertType.INFORMATION);
        				hinweis.setHeaderText("Regelwidriger Zug");
        				hinweis.setContentText("Linien duerfen sich nicht kreuzen");
        				hinweis.showAndWait();
            			fenster.setStatustext(2);
            			return;
            		}
        		}
        	}
    	}
    }
    
    /**
     * Prueft, ob alle Inseln die richtige Anzahl an ausgehenden Verbindungen in allen
     * Himmelsrichtungen hat ( Raetsel ist dann geloest )
     */
    private void istGeloest(){

        for(int i = 0; i < fenster.getInseln().length; i++) {
            for(int j = 0; j < fenster.getInseln()[i].length; j++) {
                if(fenster.getInseln()[i][j] != null) {
                	
                	if(fenster.getInseln()[i][j].aktuelleVerbindungen == fenster.getInseln()[i][j].maxAnzahlVerbindungen){
                    	// Statusupdate : "geloest"
                		fenster.setStatustext(0);
                	}
                }
            }
        }
    }
    
    /**
     * Prueft, ob die Inseln ungeloest sind, indem verglichen wird, ob die Anzahl der aktuellen
     * ausgehenden Verbindungen (nach Nord,Ost,Sued,West falls moeglich) nicht mit den Maximalwert 
     * der jeweiligen Insel uebereinstimmt.
     */
    private void istUngeloest(){
    	
        for(int i = 0; i < fenster.getInseln().length; i++) {
            for(int j = 0; j < fenster.getInseln()[i].length; j++) {
                if(fenster.getInseln()[i][j] != null) {
                	
                	if(fenster.getInseln()[i][j].aktuelleVerbindungen != fenster.getInseln()[i][j].maxAnzahlVerbindungen){
                    	// Statusupdate : "noch nicht geloest"
                		fenster.setStatustext(1);
                	}
                }
            }
        }
    }
    
    private boolean schneidet(Line eins, Line zwei){
    	/* eins = horizontal, zwei = vertikal */
    	if ( eins.getStartX() == eins.getEndX() && zwei.getStartY() == zwei.getEndY() ){
    		/* pruefe auf Kreuzung */
    		if ( eins.getStartY() <= zwei.getStartY() && zwei.getStartY() <= eins.getEndY() &&
    			 zwei.getStartX() <= eins.getStartX() && eins.getStartX() <= zwei.getEndX() ){
    			return true;
    		}
    	}
    	
    	/* eins = vertikal, zwei = horizontal */
    	if( eins.getStartY() == eins.getEndY() && zwei.getStartX() == zwei.getEndX() ){
    		if( eins.getStartX() <= zwei.getStartX() && zwei.getStartX() <= eins.getEndX() &&
    			zwei.getStartY() <= eins.getStartY() && eins.getStartY() <= zwei.getEndY() ){
    			return true;
    		}
    	}
    	return false;
    }

	private class MausHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent me) {			
			int x, y, xm, ym = 0;
			x = (int) me.getX();
			y = (int) me.getY();			
			xm = x / D;
			ym = y / D;
			
			if ( me.getButton() == MouseButton.PRIMARY){
				analysiereKlickSegment(x,y,xm,ym, true);
				
			} else if ( me.getButton() == MouseButton.SECONDARY){
				analysiereKlickSegment(x,y,xm,ym, false);				
			}
		}

	} // MausHandler ende
	
}
