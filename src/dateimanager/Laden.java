package dateimanager;

import hashi.Insel;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import gui.Fenster;

/**
 * Diese Klasse ist fuer das Laden eines gespeicherten Spielstandes zustaendig
 * 
 * @author Sebastian
 *
 */
public class Laden {

	private Fenster fenster;
	private FileChooser fc;
	private String zeile = "";
	private String eingelesenerText = new String();	
	
	public Laden(Fenster f){
		fenster = f;
		fc = new FileChooser();
		fc.setTitle("Spielstand Oeffnen...");
		fc.getExtensionFilters().addAll(new ExtensionFilter("HRD-Datei", "*.hrd"));	
		spielLaden();
	}
	
	private void spielLaden() {
		File datei = fc.showOpenDialog(fenster);
		
		if ( datei != null ){
			try {		
				BufferedReader br = new BufferedReader(new FileReader(datei.getPath()));
						
				while ((zeile = br.readLine()) != null ){
					eingelesenerText += zeile+" ";
				}
				
				if ( br != null){
					br.close();
				}
			} catch ( Exception e){
				Alert f = new Alert(AlertType.ERROR);
				f.setHeaderText("Einlesefehler");
				f.setContentText("Fehler beim einlesen des Spielstandes");
				f.showAndWait();
			}
		} else {
			return;
		}
		
        Lexer lexer = new Lexer(eingelesenerText);
        int tokensAusParser = 0;
        
        Parser parser = new Parser(lexer);
        parser.parseText();
        tokensAusParser = parser.getAnzahlTokensAusParser();
		
        /* Prï¿½fe, ob ein Syntaxfehler vorliegt */
        if ( tokensAusParser < lexer.getAnzahlTokensAusString()){
			Alert fehler = new Alert(AlertType.ERROR);
			fehler.setHeaderText("Einlesefehler");
			fehler.setContentText("Es gab ein Fehler beim einlesen der Datei. (Syntaxfehler)");
			fehler.showAndWait();
        } else {
        	uebertrageDaten();
        }
	}
	
	private void uebertrageDaten() throws NumberFormatException {
		String t = eingelesenerText;
		t = t.replace("#FELD", "");
		t = t.replace("#GENINSEL", "");
		t = t.replace("#AKTINSEL", "");
		t = t.replace("#LINIE", "");
		t = t.replace("<", " ");
		t = t.replace(",", " ");
		t = t.replace(">", " ");
		t = t.replace("(", " ");
		t = t.replace(")", " ");
		
		StringTokenizer token = new StringTokenizer(t);
		final int TOKENLAENGE = token.countTokens();
		int[] zahlen = new int[token.countTokens()];

		/* Wandel die Strings in Zahlen um und speichere Sie in das Array */
		for(int i = 0; i < TOKENLAENGE; i++){
			zahlen[i] = Integer.parseInt(token.nextToken());
		}

		if ( !speichereGeladeneDatenAb(zahlen) ){
			Alert fehler = new Alert(AlertType.ERROR);
			fehler.setHeaderText("Einlesefehler");
			fehler.setContentText("Es gab ein Fehler beim einlesen der Datei. (Semantikfehler)");
			fehler.showAndWait();
		}
	}
	
	private boolean speichereGeladeneDatenAb(int[] zahlen){
		int idx = 0;
		int breite = 0;
		int hoehe = 0;
		if (zahlen[0] >= 4 & zahlen[0] <= 30 & zahlen[1] >= 4 & zahlen[1] <= 30){
			breite = zahlen[0];
			hoehe = zahlen[1];	
		} else {
			return false;
		}
		int anzahlInseln = zahlen[2];
		ArrayList<Line> linien = new ArrayList<Line>();
		
		Insel[][] loesung = new Insel[breite][hoehe];
		Insel[][] inseln = new Insel[breite][hoehe];
		
		/* Lï¿½sung */
		idx += 3;
		for(int i = idx; i < idx + 7*anzahlInseln; i += 7){
			if ( zahlen[i] >= 0 & zahlen[i] <= 39 & zahlen[i+1] >= 0 & zahlen[i+1] <= 39 & zahlen[i+2] >= 0 & zahlen[i+2] <= 2 & zahlen[i+3] >= 0 & zahlen[i+2] <= 3 & zahlen[i+4] >= 0 & zahlen[i+4] <= 2 & zahlen[i+5] >= 0 & zahlen[i+5] <= 2 & zahlen[i+6] >= 0 & zahlen[i+6] <= 8){
				loesung[zahlen[i]][zahlen[i+1]] = new Insel(zahlen[i],zahlen[i+1],zahlen[i+6]);
				loesung[zahlen[i]][zahlen[i+1]].setVerbindungNord(zahlen[i+2]);
				loesung[zahlen[i]][zahlen[i+1]].setVerbindungOst(zahlen[i+3]);
				loesung[zahlen[i]][zahlen[i+1]].setVerbindungSued(zahlen[i+4]);
				loesung[zahlen[i]][zahlen[i+1]].setVerbindungWest(zahlen[i+5]);	
			} else {
				return false;
			}
		}
		
		/* Inseln */
		idx += 7*anzahlInseln;
		for(int i = idx; i < idx+7*anzahlInseln; i += 7){
			if ( zahlen[i] >= 0 & zahlen[i] <= 39 & zahlen[i+1] >= 0 & zahlen[i+1] <= 39 & zahlen[i+2] >= 0 & zahlen[i+2] <= 2 & zahlen[i+3] >= 0 & zahlen[i+2] <= 3 & zahlen[i+4] >= 0 & zahlen[i+4] <= 2 & zahlen[i+5] >= 0 & zahlen[i+5] <= 2 & zahlen[i+6] >= 0 & zahlen[i+6] <= 8){
				inseln[zahlen[i]][zahlen[i+1]] = new Insel(zahlen[i],zahlen[i+1],zahlen[i+6]);
				inseln[zahlen[i]][zahlen[i+1]].setVerbindungNord(zahlen[i+2]);
				inseln[zahlen[i]][zahlen[i+1]].setVerbindungOst(zahlen[i+3]);
				inseln[zahlen[i]][zahlen[i+1]].setVerbindungSued(zahlen[i+4]);
				inseln[zahlen[i]][zahlen[i+1]].setVerbindungWest(zahlen[i+5]);
			} else {
				return false;
			}
		}
		
		/* Linien */
		idx += 7*anzahlInseln;
		if ( idx == zahlen.length){
			fenster.setDaten(inseln,loesung,linien,breite,hoehe);
		} else if ( idx < zahlen.length) {
			for(int i = idx; i < zahlen.length; i += 4){
				linien.add(new Line(zahlen[i],zahlen[i+1],zahlen[i+2],zahlen[i+3]));
			}		
			fenster.setDaten(inseln,loesung,linien,breite,hoehe);
		}
	return true;
	}
}
